﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema1
{
    class Program
    {
        /// <summary>
        /// Método inicial. Genera automáticamente 10 técnicos y llama al menú principal.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.SetWindowSize(140, 30);
            List<Cliente> ListaClientes = new List<Cliente>();
            List<Tecnico> ListaTecnicos = new List<Tecnico>();
            string[] Nombres = new string[10] { "Pepe", "Juan", "Alberto", "Javi", "Mirko", "Cristian", "Jose", "Manuel", "Carlos", "Sergio" };
            for (int i = 0; i < 10; i++)
            {
                Tecnico tecnico = new Tecnico(Nombres[i], null);
                ListaTecnicos.Add(tecnico);
            }
            Menu menu = new Menu(ListaClientes,ListaTecnicos);
        }
    }
}
