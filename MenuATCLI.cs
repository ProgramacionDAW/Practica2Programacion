﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Drawing;
using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Problema1
{
    class MenuATCLI
    {
        #region Atributos
        char Opcion;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor base del menu de Atención al Cliente
        /// </summary>
        /// <param name="ListaClientes"></param>
        public MenuATCLI(List<Cliente> ListaClientes)
        {
            Menu(ListaClientes);
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Método que genera un cliente nuevo
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="cliente"></param>
        public void ClienteNuevo(List<Cliente> ListaClientes, Cliente cliente)
        {
            Console.Clear();
            cliente = AgregarCliente(ListaClientes,cliente);
            Historial historial = cliente.ObtenerHistorial();
            AgregarPeticion(historial);
        }

        /// <summary>
        /// Método que muestra el menú del cliente existente
        /// </summary>
        /// <param name="cliente"></param>
        public void ClienteExistente(Cliente cliente)
        {
            char opcion;
            Console.Clear();
            Historial historial = cliente.ObtenerHistorial();
            do
            {
                EscribirMenuCliEx();
                while (!char.TryParse(Console.ReadLine(), out opcion))
                {
                    LimpiarLinea();
                    Console.Write("Seleccione una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nSeleccione una opción: ");
                }
                switch (opcion)
                {
                    case '1':
                        if (ComprobarCliente(cliente))
                        {
                            Console.Clear();
                            Console.WriteLine("No se puede agregar una queja siendo un cliente nuevo", Color.Red);
                            Console.WriteLine("Pulsa enter para volver...");
                            Console.ReadLine();
                        }
                        else
                        {
                            AgregarQueja(historial);
                        }
                        break;
                    case '2':
                        AgregarPeticion(historial);
                        break;
                    case '3':
                        VerCliente(cliente);
                        break;
                    case '4':
                        cliente.ModificarDatos();
                        break;
                }
            } while (opcion != '0');
        }

        /// <summary>
        /// Método que nos muestra la lista de clientes en 2 formatos
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerListaClientes(List<Cliente> ListaClientes)
        {
            char opcion;
            if (ListaClientes.Count>0)
            {
                do
                {
                    EscribirMenuVer();
                    string formato = "Ninguno";
                    while (!char.TryParse(Console.ReadLine(), out opcion))
                    {
                        LimpiarLinea();
                        Console.Write("Seleccione una opción: ");
                        Console.Write("Dato introducido no valido.",Color.Red);
                        Thread.Sleep(2000);
                        LimpiarLinea();
                        Console.Write("\nSeleccione una opción: ");
                    }
                    switch (opcion)
                    {
                        case '1':
                            formato = "Detallado";
                            ListarClientes(ListaClientes, formato);
                            break;
                        case '2':
                            formato = "Simplificado";
                            ListarClientes(ListaClientes, formato);
                            break;
                    }
                } while (opcion != '0');
            }
            else
            {
                Console.Clear();
                Console.WriteLine("No hay ningun cliente registrado, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
        }

        /// <summary>
        /// Método que nos muestra el historial de un cliente concreto
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerHistorialCli(List<Cliente> ListaClientes)
        {
            Console.Title = "Historial de Cliente";
            Console.Clear();
            if (ListaClientes.Count <= 0)
            {
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            Console.Clear();
            historial.MostrarHistorial(cliente.Nombre, cliente.ID, cliente.Tecnico);
            Console.WriteLine("Pulsa enter para volver..");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que nos permite ver los datos de un cliente en 2 formatos
        /// </summary>
        /// <param name="cliente"></param>
        public void VerCliente(Cliente cliente)
        {
            char opcion;
            do
            {
                EscribirMenuVer();
                string formato = "Ninguno";
                while (!char.TryParse(Console.ReadLine(), out opcion))
                {
                    LimpiarLinea();
                    Console.Write("Seleccione una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nSeleccione una opción: ");
                }
                switch (opcion)
                {
                    case '1':
                        formato = "Detallado";
                        Console.WriteLine();
                        cliente.MostrarDatos(formato);
                        Console.WriteLine("Pulse enter para continuar...");
                        Console.ReadLine();
                        break;
                    case '2':
                        formato = "Simplificado";
                        Console.WriteLine();
                        cliente.MostrarDatos(formato);
                        Console.WriteLine("Pulse enter para continuar...");
                        Console.ReadLine();
                        break;
                }
            } while (opcion != '0');
        }

        /// <summary>
        /// Método que muestra el menú de atención al cliente
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void Menu(List<Cliente> ListaClientes)
        {
            do
            {
                EscribirMenu();
                while (!char.TryParse(Console.ReadLine(), out Opcion))
                {
                    LimpiarLinea();
                    Console.Write("Seleccione una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nSeleccione una opción: ");
                }
                switch (Opcion)
                {
                    case '1':
                        IdentificarCliente(ListaClientes);
                        break;
                    case '2':
                        VerListaClientes(ListaClientes);
                        break;
                    case '3':
                        VerHistorialCli(ListaClientes);
                        break;
                }
            } while (Opcion != '0');
        }

        /// <summary>
        /// Método que comprueba si un cliente existe o no y llama al metodo necesario
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void IdentificarCliente(List<Cliente> ListaClientes)
        {
            Console.Clear();
            Cliente cliente = BuscarCliente(ListaClientes);
            if (cliente == null)
            {
                ClienteNuevo(ListaClientes, cliente);
            }
            else
            {
                ClienteExistente(cliente);
            };
        }
        #endregion

        #region Metodos privados
        /// <summary>
        /// Método que escribe el menú por consola
        /// </summary>
        private void EscribirMenu()
        {
            Console.Title = "Menú de Atención al Cliente";
            Console.Clear();
            Console.WriteLine("=== Menú de Atención al Cliente ===");
            Console.WriteLine("1 - Identificar Cliente.");
            Console.WriteLine("2 - Ver lista de Clientes.");
            Console.WriteLine("3 - Ver Historial de cliente");
            Console.WriteLine("0 - Volver");
            Console.Write("\nSeleccione una opción: ");
        }

        /// <summary>
        /// Método que nos permite seleccionar un cliente concreto
        /// </summary>
        /// <param name="lista">Lista de Clientes</param>
        /// <returns>Devuelve el cliente en caso de que exista</returns>
        private Cliente SeleccionarCliente(List<Cliente> lista)
        {
            int seleccion;
            int contador = 0;
            Console.WriteLine("Mostrando listado de clientes");
            for (int i = 0; i < lista.Count; i++)
            {
                Cliente cliente = lista[i];
                string[] datos = new string[3] { (i + 1).ToString(), cliente.DNI, cliente.Nombre };
                string esquema = "Numero: {0} - DNI: {1} - Nombre: {2}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
                contador++;
            }
            Console.Write("\nEscriba el numero del cliente a seleccionar: ");
            while (!int.TryParse(Console.ReadLine(), out seleccion))
            {
                LimpiarLinea();
                Console.Write("Escriba el numero del cliente a seleccionar: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nEscriba el numero del cliente a seleccionar: ");
            }
            if (seleccion < 1 || seleccion > contador)
            {
                return null;
            }
            seleccion--;
            return lista[seleccion];
        }

        /// <summary>
        /// Método que escribe por consola el menú de cliente existente
        /// </summary>
        private void EscribirMenuCliEx()
        {
            Console.Title = "Cliente existente";
            Console.Clear();
            Console.WriteLine("=== Cliente existente ===");
            Console.WriteLine("1. Queja o reclamación");
            Console.WriteLine("2. Nuevo trabajo");
            Console.WriteLine("3. Ver datos del cliente");
            Console.WriteLine("4. Modificar datos del cliente");
            Console.WriteLine("0. Volver");
            Console.Write("\nSeleccione una opción: ");
        }

        /// <summary>
        /// Método que comprueba si un cliente existente es nuevo o no (usado para comprobar si puede solicitar quejas)
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns>Devuelve true si el cliente es nuevo (Ningun trabajo finalizado) o false en caso contrario</returns>
        private bool ComprobarCliente(Cliente cliente)
        {
            Historial historial = cliente.ObtenerHistorial();
            List<Peticion> trabajos = historial.ObtenerPeticiones();
            foreach (Peticion peticion in trabajos)
            {
                if (peticion.Tipo== "Trabajo" && peticion.Estado== "Terminada")
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Método que genera un nuevo objeto de tipo Cliente y su Historial
        /// </summary>
        /// <param name="lista">Lista de Clientes</param>
        /// <param name="cliente"></param>
        /// <returns></returns>
        private Cliente AgregarCliente(List<Cliente>lista, Cliente cliente)
        {
            Console.Title = "Cliente nuevo";
            Console.Clear();
            Console.WriteLine("=== Cliente nuevo ===");
            Console.WriteLine("Se procederá a crear un nuevo cliente");
            Console.Write("\nNombre completo: ");
            string Nombre = Console.ReadLine();
            Console.Write("\nDNI: ");
            string DNI = Console.ReadLine();
            while (BuscarDNI(DNI,lista))
            {
                LimpiarLinea();
                Console.Write("DNI: ");
                Console.Write("El DNI ya existe.", Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nDNI: ");
                DNI = Console.ReadLine();
            }
            Console.Write("\nDirección: ");
            string Direccion = Console.ReadLine();
            Console.Write("\nTelefono: ");
            int Telefono;
            while (!int.TryParse(Console.ReadLine(), out Telefono))
            {
                LimpiarLinea();
                Console.Write("Telefono: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nTelefono: ");
            }
            Console.Write("\nCuenta bancaria: ");
            string CuentaBancaria = Console.ReadLine();
            int ID = lista.Count + 1;
            Historial historial = new Historial();
            cliente = new Cliente(ID,DNI,Nombre,Direccion,Telefono,CuentaBancaria,historial);
            lista.Add(cliente);
            Console.WriteLine();
            return cliente;
        }

        /// <summary>
        /// Método que comprueba si existe un DNI
        /// </summary>
        /// <param name="DNI"></param>
        /// <param name="lista">Lista de Clientes</param>
        /// <returns>True si el DNI existe, False en caso contrario</returns>
        private bool BuscarDNI(string DNI, List<Cliente> lista)
        {
            foreach (Cliente cliente in lista)
            {
                if (cliente.DNI==DNI)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Método que busca un cliente por su DNI
        /// </summary>
        /// <param name="lista">Lista de Clientes</param>
        /// <returns>Devuelve el cliente en caso de que exista</returns>
        private Cliente BuscarCliente(List<Cliente> lista)
        {
            Console.Write("Escriba el DNI del cliente a buscar: ");
            string DNI = Console.ReadLine();
            foreach (Cliente cliente in lista)
            {
                if (cliente.DNI==DNI)
                {
                    return cliente;
                }
            }
            return null;
        }

        /// <summary>
        /// Método que agrega una nueva queja al historial del cliente
        /// </summary>
        /// <param name="historial"></param>
        private void AgregarQueja(Historial historial)
        {
            Console.Clear();
            Console.Title = "Queja o Reclamación";
            Console.WriteLine("\t=== Agregar queja o reclamación ===");
            Console.Write("\nEscriba la descripción de la queja: ");
            string descripcion = Console.ReadLine();
            string id = historial.AgregarQueja(descripcion);
            historial.RealizarActuacion("Atención al cliente", id, "Queja", "Agregada nueva queja o reclamación");
            Console.WriteLine("Queja o Reclamación '{0}' enviada al departamento de Calidad.",id);
            Console.WriteLine("Pulsa enter para volver...");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que agrega un nuevo Trabajo al historial del cliente
        /// </summary>
        /// <param name="historial"></param>
        private void AgregarPeticion(Historial historial)
        {
            Console.Clear();
            Console.Title = "Petición de trabajo";
            Console.WriteLine("\t=== Agregar petición de trabajo ===");
            Console.Write("\nEscriba la descripción del trabajo: ");
            string descripcion = Console.ReadLine();
            string id = historial.AgregarPeticion(descripcion);
            historial.RealizarActuacion("Atención al cliente", id, "Trabajo", "Agregada nueva petición de trabajo");
            Console.WriteLine("Petición '{0}' enviada al departamento de Desarrollo.",id);
            Console.WriteLine("Pulse enter para volver...");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que escribe el menu de selección de vista
        /// </summary>
        private void EscribirMenuVer()
        {
            Console.Title = "Seleccion de vista";
            Console.Clear();
            Console.WriteLine("\t=== Tipo de vista ===");
            Console.WriteLine("1. Detallada");
            Console.WriteLine("2. Simplificada");
            Console.WriteLine("0. Volver");
            Console.Write("\nSeleccione una opción: ");
        }

        /// <summary>
        /// Método que muestra la lista de clientes en 2 formatos distintos
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="formato"></param>
        private void ListarClientes(List<Cliente> ListaClientes,string formato)
        {
            Console.Clear();
            Console.WriteLine("Mostrando lista de clientes en formato {0}", formato);
            Console.WriteLine();
            switch (formato)
            {
                case "Simplificado":
                    foreach (Cliente cliente in ListaClientes)
                    {
                        cliente.MostrarDatos(formato);
                    }
                    break;
                case "Detallado":
                    foreach (Cliente cliente in ListaClientes)
                    {
                        cliente.MostrarDatos(formato);
                    }
                    break;
            }
            Console.WriteLine("Pulse enter para continuar...");
            Console.ReadLine();
        }

        /// <summary>
        /// Metodo que limpia la linea (en la que se ejecuta) de la consola
        /// </summary>
        private void LimpiarLinea()
        {
            int lineaActual = Console.CursorTop - 1;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, lineaActual);
        }
        #endregion

        #region Propiedades
        #endregion

        #region Indizadores
        #endregion
    }
}
