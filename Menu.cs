﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Drawing;
using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Problema1
{
    class Menu
    {
        #region Atributos
        char Opcion;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor base de la clase Menú
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public Menu(List<Cliente>ListaClientes,List<Tecnico>ListaTecnicos)
        {
            MostrarMenu(ListaClientes, ListaTecnicos);
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Método que muestra el menu principal
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public void MostrarMenu(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            do
            {
                EscribirMenu();
                Console.Write("\nPulse una opción: ");
                while (!char.TryParse(Console.ReadLine(),out Opcion))
                {
                    LimpiarLinea();
                    Console.Write("Pulse una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nPulse una opción: ");
                }
                switch (Opcion)
                {
                    case '1':
                        MenuATCLI menuAt = new MenuATCLI(ListaClientes);
                        break;
                    case '2':
                        MenuDes menuDes = new MenuDes(ListaClientes,ListaTecnicos);
                        break;
                    case '3':
                        MenuCal menuCal = new MenuCal(ListaClientes,ListaTecnicos);
                        break;
                }
            } while (Opcion != '0');
        }
        #endregion

        #region Metodos privados
        /// <summary>
        /// Método que escribe por consola el menú principal
        /// </summary>
        private void EscribirMenu()
        {
            Console.Title = "Menú Principal";
            Console.Clear();
            Console.WriteLine("=== Menú Principal ===");
            Console.WriteLine("1 - Atención al Cliente");
            Console.WriteLine("2 - Desarrollo");
            Console.WriteLine("3 - Calidad");
            Console.WriteLine("0 - Salir");
        }
        //private void AgregarTecnicosPrueba(List<Tecnico> ListaTecnicos)
        //{
        //    Console.Clear();
        //    string[] Nombres = new string[10] { "Pepe", "Juan", "Alberto", "Javi", "Mirko", "Cristian", "Jose", "Manuel", "Carlos", "Sergio"};
        //    for (int i = 0; i<10;i++)
        //    {
        //        Tecnico tecnico = new Tecnico(Nombres[i],null);
        //        ListaTecnicos.Add(tecnico);
        //        Console.WriteLine("Agregado tecnico {0}", Nombres[i]);
        //    }
        //    Console.WriteLine("Pulsa enter para volver...");
        //    Console.ReadLine();
        //}

        /// <summary>
        /// Metodo que limpia la linea (en la que se ejecuta) de la consola
        /// </summary>
        private void LimpiarLinea()
        {
            int lineaActual = Console.CursorTop-1;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, lineaActual);
        }
        #endregion

        #region Propiedades
        #endregion

        #region Indizadores
        #endregion
    }
}
