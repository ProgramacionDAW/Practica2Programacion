﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Drawing;
using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Problema1
{
    class MenuDes
    {
        #region Atributos
        char Opcion;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor base del menú de Desarrollo
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public MenuDes(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            MostrarMenu(ListaClientes,ListaTecnicos);
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Método que muestra el menú de Desarrollo
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public void MostrarMenu(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            do
            {
                EscribirMenu();
                Console.Write("\nPulse una opción: ");
                while (!char.TryParse(Console.ReadLine(), out Opcion))
                {
                    LimpiarLinea();
                    Console.Write("Pulse una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nPulse una opción: ");
                }
                switch (Opcion)
                {
                    case '1':
                        AsignarTecnico(ListaClientes, ListaTecnicos);
                        break;
                    case '2':
                        RealizarActuacion(ListaClientes);
                        break;
                    case '3':
                        FinalizarTrabajo(ListaClientes);
                        break;
                    case '4':
                        VerHistorialCli(ListaClientes);
                            break;
                    case '5':
                        VerListaClientes(ListaClientes);
                        break;
                    case '6':
                        VerListaTecnicos(ListaTecnicos);
                        break;
                }
            } while (Opcion != '0');
        }

        /// <summary>
        /// Método que asigna un técnico a un cliente
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public void AsignarTecnico(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            Console.Title = "Asignación de Tecnico";
            Console.Clear();
            Console.WriteLine("\t=== Asignación de Tecnico === (Paso 1/3)");
            if (ListaClientes.Count<=0)
            {
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            if (ContarTrabajosAct(historial)==0)
            {
                Console.WriteLine("El cliente no tiene ninguna petición de trabajo abierta, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Console.Clear();
            Console.WriteLine("\t=== Asignación de Tecnico === (Paso 2/3)");
            if (ListaTecnicos.Count<=0)
            {
                Console.WriteLine("No hay tecnicos registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Tecnico tecnico;
            if (cliente.Tecnico!= "No asignado")
            {
                tecnico = BuscarTecnico(ListaTecnicos, cliente.Tecnico);
                if (tecnico.Estado != "Libre")
                {
                    tecnico = SeleccionarTecnico(ListaTecnicos);
                    while (tecnico == null)
                    {
                        Console.WriteLine("Tecnico invalido, pulse intro para volver a seleccionar un tecnico...", Color.Red);
                        Console.ReadLine();
                        Console.Clear();
                        tecnico = SeleccionarTecnico(ListaTecnicos);
                    }
                }
                cliente.AsignarTecnico(tecnico);
                tecnico.AsignarCliente(cliente);
            }
            else
            {
                tecnico = SeleccionarTecnico(ListaTecnicos);
                while (tecnico == null)
                {
                    Console.WriteLine("Tecnico invalido, pulse intro para volver a seleccionar un tecnico...", Color.Red);
                    Console.ReadLine();
                    Console.Clear();
                    tecnico = SeleccionarTecnico(ListaTecnicos);
                }
                cliente.AsignarTecnico(tecnico);
                tecnico.AsignarCliente(cliente);
            }
            Console.Clear();
            Console.WriteLine("\t=== Asignación de Tecnico === (Paso 3/3)");
            Peticion peticion = SeleccionarTrabajo(historial);
            while (peticion==null)
            {
                Console.WriteLine("Petición seleccionada invalida, pulse enter para volver a seleccionar...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                peticion = SeleccionarTrabajo(historial);
            }
            peticion.AsignarTrabajo(tecnico);
            historial.RealizarActuacion("Dpto Desarrollo", peticion.ID, peticion.Tipo, "Asignación de tecnico");
            Console.WriteLine("Pulsa enter para continuar...");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que agrega una actuación (mensaje) al historial del cliente
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void RealizarActuacion(List<Cliente> ListaClientes)
        {
            Console.Title = "Realizar actuación";
            Console.Clear();
            if (ListaClientes.Count <= 0)
            {
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            Console.Clear();
            if (ContarTrabajosActivos(historial) == 0)
            {
                Console.WriteLine("El cliente seleccionado no tiene ningun trabajo activo.",Color.Red);
                Console.WriteLine("Volviendo al menu...");
                Thread.Sleep(2000);
                return;
            }
            Peticion trabajo = SeleccionarTrabajoAct(historial);
            Console.Clear();
            Console.WriteLine("Agregando actuación a la petición {0} como {1}", trabajo.ID, trabajo.Tecnico);
            Console.Write("\nEscriba el contenido de la actuación: ");
            string mensaje = Console.ReadLine();
            historial.RealizarActuacion(trabajo.Tecnico, trabajo.ID, "Trabajo", mensaje);
            Console.WriteLine("Actuación agregada al historial del cliente.");
            Console.WriteLine("Pulse enter para volver...");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que finaliza una petición de trabajo de un cliente
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void FinalizarTrabajo(List<Cliente> ListaClientes)
        {
            Console.Title = "Finalizar trabajo";
            Console.Clear();
            if (ListaClientes.Count <= 0)
            {
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Console.Clear();
            Historial historial = cliente.ObtenerHistorial();
            if (ContarTrabajosActivos(historial) == 0)
            {
                Console.WriteLine("El cliente seleccionado no tiene ningun trabajo activo.",Color.Red);
                Console.WriteLine("Volviendo al menu...");
                Thread.Sleep(2000);
                return;
            }
            Peticion trabajo = SeleccionarTrabajoAct(historial);
            Console.Clear();
            historial.RealizarActuacion(trabajo.Tecnico, trabajo.ID, trabajo.Tipo, "Trabajo finalizado.");
            trabajo.FinalizarTrabajo();
            Console.WriteLine("Finalizado el trabajo {0} del cliente {1}", trabajo.ID, cliente.Nombre);
            Console.WriteLine("El estado del tecnico {0} es ahora Libre",trabajo.Tecnico);
            Console.WriteLine("Pulsa enter para salir...");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que muestra el historial de un cliente concreto
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerHistorialCli(List<Cliente> ListaClientes)
        {
            Console.Title = "Historial de Cliente";
            Console.Clear();
            if (ListaClientes.Count <= 0)
            {
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            Console.Clear();
            historial.MostrarHistorial(cliente.Nombre,cliente.ID,cliente.Tecnico);
            Console.WriteLine("Pulsa enter para volver..");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que muestra la lista de clientes en 2 formatos distintos
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerListaClientes(List<Cliente> ListaClientes)
        {
            if (ListaClientes.Count<=0)
            {
                Console.Clear();
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            char opcion;
            do
            {
                EscribirMenuVer();
                string formato = "Ninguno";
                while (!char.TryParse(Console.ReadLine(), out opcion))
                {
                    LimpiarLinea();
                    Console.Write("Seleccione una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nSeleccione una opción: ");
                }
                switch (opcion)
                {
                    case '1':
                        formato = "Detallado";
                        ListarClientes(ListaClientes, formato);
                        break;
                    case '2':
                        formato = "Simplificado";
                        ListarClientes(ListaClientes, formato);
                        break;
                }
            } while (opcion != '0');
        }

        /// <summary>
        /// Método que muestra la lista de técnicos con su estado
        /// </summary>
        /// <param name="ListaTecnicos"></param>
        public void VerListaTecnicos(List<Tecnico> ListaTecnicos)
        {
            Console.Title = "Lista de Tecnicos";
            Console.Clear();
            if (ListaTecnicos.Count<=0)
            {
                Console.WriteLine("No hay tecnicos registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            int posx = 7;
            int posy = 4;
            int contador = 1;
            Console.WriteLine("Mostrando lista de tecnicos:");
            Console.SetCursorPosition(5,3);
            Console.Write("┌────────────────────────────────────────────────────────────────┐",Color.LightSkyBlue);
            Console.SetCursorPosition(5,4);
            Console.Write("│            │            │            │            │            │", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 5);
            Console.Write("│────────────│────────────│────────────│────────────│────────────│", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 6);
            Console.Write("│            │            │            │            │            │", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 7);
            Console.Write("└────────────────────────────────────────────────────────────────┘", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 10);
            Console.Write("┌────────────────────────────────────────────────────────────────┐", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 11);
            Console.Write("│            │            │            │            │            │", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 12);
            Console.Write("│────────────│────────────│────────────│────────────│────────────│", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 13);
            Console.Write("│            │            │            │            │            │", Color.LightSkyBlue);
            Console.SetCursorPosition(5, 14);
            Console.Write("└────────────────────────────────────────────────────────────────┘", Color.LightSkyBlue);
            foreach (Tecnico tecnico in ListaTecnicos)
            {
                if (contador<=5)
                {
                    Console.SetCursorPosition(posx, posy);
                    Console.Write(tecnico.Nombre,Color.Yellow);
                    posy = posy + 2;
                    Console.SetCursorPosition(posx, posy);
                    if (tecnico.Estado== "Libre")
                    {
                        Console.Write(tecnico.Estado,Color.LightGreen); 
                    }
                    else
                    {
                        Console.Write(tecnico.Estado, Color.IndianRed);
                    }
                    posy = posy - 2;
                    posx = posx + 13;
                    contador++;
                }
                else if (contador==6)
                {
                    posx = 7;
                    posy = 11;
                    Console.SetCursorPosition(posx, posy);
                    Console.Write(tecnico.Nombre, Color.Yellow);
                    posy = posy + 2;
                    Console.SetCursorPosition(posx, posy);
                    if (tecnico.Estado == "Libre")
                    {
                        Console.Write(tecnico.Estado, Color.LightGreen);
                    }
                    else
                    {
                        Console.Write(tecnico.Estado, Color.IndianRed);
                    }
                    posy = posy - 2;
                    posx = posx + 13;
                    contador++;
                }
                else
                {
                    Console.SetCursorPosition(posx, posy);
                    Console.Write(tecnico.Nombre, Color.Yellow);
                    posy = posy + 2;
                    Console.SetCursorPosition(posx, posy);
                    if (tecnico.Estado == "Libre")
                    {
                        Console.Write(tecnico.Estado, Color.LightGreen);
                    }
                    else
                    {
                        Console.Write(tecnico.Estado, Color.IndianRed);
                    }
                    posy = posy - 2;
                    posx = posx + 13;
                    contador++;
                }
            }
            Console.SetCursorPosition(0, 16);
            Console.Write("Pulsa enter para volver...");
            Console.ReadLine();
        }
        #endregion

        #region Metodos privados
        /// <summary>
        /// Método que escribe por consola el menú de Desarrollo
        /// </summary>
        private void EscribirMenu()
        {
            Console.Title = "Menú de Desarrollo";
            Console.Clear();
            Console.WriteLine("=== Menú de Desarrollo ===");
            Console.WriteLine("1 - Asignar tecnico");
            Console.WriteLine("2 - Realizar actuación");
            Console.WriteLine("3 - Finalizar petición");
            Console.WriteLine("4 - Ver Historial de cliente");
            Console.WriteLine("5 - Ver Clientes");
            Console.WriteLine("6 - Ver Tecnicos");
            Console.WriteLine("0 - Volver");
        }

        /// <summary>
        /// Método que busca un técnico por su nombre
        /// </summary>
        /// <param name="ListaTecnicos"></param>
        /// <param name="nombre">Nombre del técnico</param>
        /// <returns>Devuelve el técnico en caso de que exista</returns>
        private Tecnico BuscarTecnico(List<Tecnico> ListaTecnicos, string nombre)
        {
            foreach (Tecnico tecnico in ListaTecnicos)
            {
                if (tecnico.Nombre == nombre)
                {
                    return tecnico;
                }
            }
            return null;
        }

        /// <summary>
        /// Método que nos permite seleccionar un cliente concreto
        /// </summary>
        /// <param name="lista">Lista de Clientes</param>
        /// <returns>Devuelve el cliente en caso de que exista</returns>
        private Cliente SeleccionarCliente(List<Cliente> lista)
        {
            int seleccion;
            int contador = 0;
            Console.WriteLine("Mostrando listado de clientes");
            for (int i = 0;i<lista.Count;i++)
            {
                Cliente cliente = lista[i];
                string[] datos = new string[3] { (i + 1).ToString(), cliente.DNI, cliente.Nombre };
                string esquema = "Numero: {0} - DNI: {1} - Nombre: {2}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
                contador++;
            }
            Console.Write("\nEscriba el numero del cliente a seleccionar: ");
            while (!int.TryParse(Console.ReadLine(), out seleccion))
            {
                LimpiarLinea();
                Console.Write("Escriba el numero del cliente a seleccionar: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nEscriba el numero del cliente a seleccionar: ");
            }
            if (seleccion < 1 || seleccion > contador)
            {
                return null;
            }
            seleccion--;
            return lista[seleccion];
        }

        /// <summary>
        /// Método que nos permite seleccionar un técnico concreto
        /// </summary>
        /// <param name="lista">Lista de Técnicos</param>
        /// <returns>Devuelve el técnico en caso de que exista</returns>
        private Tecnico SeleccionarTecnico(List<Tecnico> lista)
        {
            int seleccion;
            int contador = 0;
            List<Tecnico> listatecnicos = new List<Tecnico>();
            Console.WriteLine("Mostrando listado de tecnicos disponibles");
            for (int i = 0; i<lista.Count;i++)
            {
                Tecnico tecnico = lista[i];
                if (tecnico.Estado == "Libre")
                {
                    listatecnicos.Add(tecnico);
                    contador++;
                }
            }
            for (int i = 0; i < listatecnicos.Count; i++)
            {
                Tecnico tecnico = listatecnicos[i];
                string[] datos = new string[2] { (i + 1).ToString(), tecnico.Nombre };
                string esquema = "Numero: {0} - Nombre: {1}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            }
            Console.WriteLine();
            Console.Write("\nEscribe el numero del tecnico a seleccionar: ");
            while (!int.TryParse(Console.ReadLine(), out seleccion))
            {
                LimpiarLinea();
                Console.Write("Escribe el numero del tecnico a seleccionar: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nEscribe el numero del tecnico a seleccionar: ");
            }
            if (seleccion < 1 || seleccion > contador)
            {
                return null;
            }
            seleccion--;
            return listatecnicos[seleccion];
        }

        /// <summary>
        /// Método que nos permite seleccionar un trabajo concreto
        /// </summary>
        /// <param name="historial"></param>
        /// <returns>Devuelve la petición de trabajo en caso de que exista</returns>
        private Peticion SeleccionarTrabajo(Historial historial)
        {
            int seleccion;
            int contador = 0;
            List<Peticion> listado = new List<Peticion>();
            Console.WriteLine("Mostrando lista de peticiones disponibles");
            List<Peticion> listaPeticiones = historial.ObtenerPeticiones();
            for (int i = 0;i<listaPeticiones.Count;i++)
            {
                Peticion peticion = listaPeticiones[i];
                if (peticion.Estado == "Abierta")
                {
                    listado.Add(peticion);
                    contador++;
                }
            }
            for (int i = 0; i < listado.Count; i++)
            {
                Peticion peticion = listado[i];
                string[] datos = new string[3] { (i + 1).ToString(), peticion.ID,peticion.Descripcion };
                string esquema = "Numero: {0} - ID: {1} - Descripción: {2}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            }
            Console.WriteLine();
            Console.Write("\nEscribe el numero de la petición a seleccionar: ");
            while (!int.TryParse(Console.ReadLine(), out seleccion))
            {
                LimpiarLinea();
                Console.Write("Escribe el numero de la petición a seleccionar: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nEscribe el numero de la petición a seleccionar: ");
            }
            if (seleccion < 1 || seleccion > contador)
            {
                return null;
            }
            seleccion--;
            return listado[seleccion];
        }

        /// <summary>
        /// Método que nos permite seleccionar un trabajo activo concreto
        /// </summary>
        /// <param name="historial"></param>
        /// <returns>Devuelve la petición de trabajo en caso de que exista</returns>
        private Peticion SeleccionarTrabajoAct(Historial historial)
        {
            int seleccion;
            int contador = 0;
            List<Peticion> listado = new List<Peticion>();
            Console.WriteLine("Mostrando lista de peticiones disponibles");
            List<Peticion> listaPeticiones = historial.ObtenerPeticiones();
            for (int i = 0; i < listaPeticiones.Count; i++)
            {
                Peticion peticion = listaPeticiones[i];
                if (peticion.Tipo=="Trabajo" && peticion.Estado == "Activa")
                {
                    listado.Add(peticion);
                    contador++;
                }
            }
            for (int i = 0; i < listado.Count; i++)
            {
                Peticion peticion = listado[i];
                string[] datos = new string[3] { (i + 1).ToString(), peticion.ID, peticion.Descripcion };
                string esquema = "Numero: {0} - ID: {1} - Descripción: {2}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            }
            Console.WriteLine();
            Console.Write("\nEscribe el numero de la petición a seleccionar: ");
            while (!int.TryParse(Console.ReadLine(), out seleccion))
            {
                LimpiarLinea();
                Console.Write("Escribe el numero de la petición a seleccionar: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nEscribe el numero de la petición a seleccionar: ");
            }
            if (seleccion < 1 || seleccion > contador)
            {
                return null;
            }
            seleccion--;
            return listado[seleccion];
        }

        /// <summary>
        /// Método que cuenta la cantidad de trabajos activos que tiene un cliente (para saber si tiene alguno)
        /// </summary>
        /// <param name="historial"></param>
        /// <returns>Devuelve la cantidad de trabajos activos</returns>
        private int ContarTrabajosActivos(Historial historial)
        {
            List<Peticion> peticiones = historial.ObtenerPeticiones();
            int contador = 0;
            foreach (Peticion peticion in peticiones)
            {
                if (peticion.Tipo == "Trabajo" && peticion.Estado== "Activa")
                {
                    contador++;
                }
            }
            return contador;
        }

        /// <summary>
        /// Método que cuenta la cantidad de trabajos abiertos que tiene un cliente (para saber si tiene alguno)
        /// </summary>
        /// <param name="historial"></param>
        /// <returns>Devuelve la cantidad de trabajos abiertos</returns>
        private int ContarTrabajosAct(Historial historial)
        {
            List<Peticion> peticiones = historial.ObtenerPeticiones();
            int contador = 0;
            foreach (Peticion peticion in peticiones)
            {
                if (peticion.Tipo == "Trabajo" && peticion.Estado== "Abierta")
                {
                    contador++;
                }
            }
            return contador;
        }

        /// <summary>
        /// Método que escribe el menú de selección de vista
        /// </summary>
        private void EscribirMenuVer()
        {
            Console.Title = "Seleccion de vista";
            Console.Clear();
            Console.WriteLine("\t=== Tipo de vista ===");
            Console.WriteLine("1. Detallada");
            Console.WriteLine("2. Simplificada");
            Console.WriteLine("0. Volver");
            Console.Write("\nSeleccione una opción: ");
        }

        /// <summary>
        /// Método que muestra la lista de clientes en 2 formatos distintos
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="formato"></param>
        private void ListarClientes(List<Cliente> ListaClientes, string formato)
        {
            Console.Clear();
            Console.WriteLine("Mostrando lista de clientes en formato {0}", formato);
            Console.WriteLine();
            switch (formato)
            {
                case "Simplificado":
                    foreach (Cliente cliente in ListaClientes)
                    {
                        cliente.MostrarDatos(formato);
                    }
                    break;
                case "Detallado":
                    foreach (Cliente cliente in ListaClientes)
                    {
                        cliente.MostrarDatos(formato);
                    }
                    break;
            }
            Console.WriteLine("Pulse enter para continuar...");
            Console.ReadLine();
        }

        /// <summary>
        /// Metodo que limpia la linea (en la que se ejecuta) de la consola
        /// </summary>
        private void LimpiarLinea()
        {
            int lineaActual = Console.CursorTop - 1;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, lineaActual);
        }
        #endregion

        #region Propiedades
        #endregion

        #region Indizadores
        #endregion
    }
}
