﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema1
{
    class Tecnico
    {
        #region Atributos
        Cliente cliente;
        string NombreT;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor básico de la clase Técnico
        /// </summary>
        public Tecnico()
        {

        }

        /// <summary>
        /// Constructor base de la clase Técnico
        /// </summary>
        /// <param name="Nombre"></param>
        /// <param name="cliente"></param>
        public Tecnico(string Nombre,Cliente cliente)
        {
            NombreT = Nombre;
            this.cliente = cliente;

        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Método que asigna un cliente al técnico
        /// </summary>
        /// <param name="cliente"></param>
        public void AsignarCliente(Cliente cliente)
        {
            this.cliente = cliente;
        }
        #endregion

        #region Metodos privados
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el nombre del técnico
        /// </summary>
        public string Nombre
        {
            get
            {
                return NombreT;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el estado del técnico (Libre o Ocupado)
        /// </summary>
        public string Estado
        {
            get
            {
                if(cliente == null)
                {
                    return "Libre";
                }
                else
                {
                    return "Ocupado";
                }
            }
        }
        #endregion

        #region Indizadores
        #endregion
    }
}
