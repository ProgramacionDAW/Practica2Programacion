﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema1
{
    class Peticion
    {
        #region Atributos
        enum estadosT { Abierta, Activa, Terminada };
        enum estadosQ { Activa, Cerrada };
        enum tipos { Trabajo, Queja };
        string fecha;
        string tipo;
        string id;
        string estado;
        string descripcion;
        Tecnico tecnico;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor basico de la clase Petición
        /// </summary>
        public Peticion()
        {

        }

        /// <summary>
        /// Constructor base de la clase Petición
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="tipo"></param>
        /// <param name="id"></param>
        /// <param name="descripcion"></param>
        public Peticion(string fecha, string tipo, string id,string descripcion)
        {
            this.fecha = fecha;
            this.tipo = tipo;
            this.id = id;
            this.descripcion = descripcion;
            if (tipo == tipos.Trabajo.ToString())
            {
                estado = estadosT.Abierta.ToString();
            }
            else
            {
                estado = estadosQ.Activa.ToString();
            }
            tecnico = null;
        }
        #endregion

        #region Metodos Publicos
        /// <summary>
        /// Método que asigna a la petición un técnico
        /// </summary>
        /// <param name="tecnico"></param>
        public void AsignarTrabajo(Tecnico tecnico)
        {
            estado = estadosT.Activa.ToString();
            this.tecnico = tecnico;
        }

        /// <summary>
        /// Método que finaliza una petición (tanto de trabajo como de queja) y elimina el cliente del objeto técnico
        /// </summary>
        public void FinalizarTrabajo()
        {
            if (tipo == tipos.Trabajo.ToString())
            {
                estado = estadosT.Terminada.ToString();
            }
            else
            {
                estado = estadosQ.Cerrada.ToString();
            }
            if (tecnico != null)
            {
                tecnico.AsignarCliente(null);
            }
        }
        #endregion

        #region Metodos Privados
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el tipo de petición (Trabajo o Queja)
        /// </summary>
        public string Tipo
        {
            get
            {
                return tipo;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la ID de la petición
        /// </summary>
        public string ID
        {
            get
            {
                return id;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el estado de la petición
        /// </summary>
        public string Estado
        {
            get
            {
                return estado;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el técnico asignado a la petición
        /// </summary>
        public string Tecnico
        {
            get
            {
                return tecnico.Nombre;
            }
        }

        /// <summary>
        /// Propiedad que devuelve la fecha de creación de la petición
        /// </summary>
        public string Fecha
        {
            get
            {
                return fecha;
            }
        }

        /// <summary>
        /// Propiedad que devuelvé la descripción de la petición
        /// </summary>
        public string Descripcion
        {
            get
            {
                return descripcion;
            }
        }
        #endregion

        #region Indizadores
        #endregion
    }
}
