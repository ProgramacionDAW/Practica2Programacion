﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Drawing;
using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;

namespace Problema1
{
    class Cliente
    {
        #region Atributos
        int ID_Cliente;
        string DNIc;
        string nombre;
        string Direccion;
        int Telefono;
        string CuentaBancaria;
        string ultimoTecnico;
        Historial historial;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor vacio de la clase Cliente
        /// </summary>
        public Cliente()
        {

        }

        /// <summary>
        /// Constructor usado normalmente para crear un cliente nuevo
        /// </summary>
        /// <param name="ID_Cliente"></param>
        /// <param name="DNI"></param>
        /// <param name="Nombre"></param>
        /// <param name="Direccion"></param>
        /// <param name="Telefono"></param>
        /// <param name="CuentaBancaria"></param>
        /// <param name="historial">Objeto que almacenará todo el historial del cliente</param>
        public Cliente(int ID_Cliente,string DNI, string Nombre, string Direccion, int Telefono, string CuentaBancaria,Historial historial)
        {
            this.ID_Cliente = ID_Cliente;
            DNIc = DNI;
            nombre = Nombre;
            this.Direccion = Direccion;
            this.Telefono = Telefono;
            this.CuentaBancaria = CuentaBancaria;
            this.historial = historial;
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Devuelve el historial del cliente
        /// </summary>
        /// <returns>Historial del cliente</returns>
        public Historial ObtenerHistorial()
        {
            return historial;
        }

        /// <summary>
        /// Asigna un tecnico al cliente
        /// </summary>
        /// <param name="tecnico">El tecnico</param>
        public void AsignarTecnico(Tecnico tecnico)
        {
            ultimoTecnico = tecnico.Nombre;
        }

        /// <summary>
        /// Muestra los datos del cliente de 2 formatos distintos
        /// </summary>
        /// <param name="formato">identifica que formato usar</param>
        public void MostrarDatos(string formato)
        {
            switch (formato)
            {
                case "Simplificado":
                    DatosSimplificado();
                    break;
                case "Detallado":
                    DatosDetallado();
                    break;
            }
        }

        /// <summary>
        /// Metodo usado para modificar los datos del cliente
        /// </summary>
        public void ModificarDatos()
        {
            Regex letras = new Regex("[a-zA-Z]+");
            Regex numeros = new Regex("[0-9]+");
            Console.Title = "Modificar datos cliente";
            Console.Clear();
            Console.WriteLine("Si no desea modificar algun dato, deje el campo vacio");
            Console.Write("\nDNI: ");
            string dni = Console.ReadLine();
            Console.Write("\nNombre: ");
            string nombre = Console.ReadLine();
            Console.Write("\nDirección: ");
            string direccion = Console.ReadLine();
            Console.Write("\nTelefono: ");
            int telf;
            while (!int.TryParse(Console.ReadLine(), out telf))
            {
                LimpiarLinea();
                Console.Write("Telefono: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nTelefono: ");
            }
            Console.Write("\nCuenta Bancaria: ");
            string cuenta = Console.ReadLine();
            if (letras.IsMatch(dni))
            {
                DNIc = dni;
            }
            if (letras.IsMatch(nombre))
            {
                this.nombre = nombre;
            }
            if (letras.IsMatch(direccion))
            {
                Direccion = direccion;
            }
            if (letras.IsMatch(cuenta))
            {
                CuentaBancaria = cuenta;
            }
            if (numeros.IsMatch(telf.ToString()))
            {
                Telefono = telf;
            }
            Console.WriteLine("Cliente modificado.");
            Console.WriteLine("Pulse enter para volver...");
            Console.ReadLine();
        }
        #endregion

        #region Metodos privados
        /// <summary>
        /// Muestra los datos de forma simplificada
        /// </summary>
        private void DatosSimplificado()
        {
            string[] datos = new string[] { ID_Cliente.ToString(), DNIc , nombre , historial.NumTrabajos.ToString() , historial.NumQuejas.ToString() };
            string esquema = "ID: {0} - DNI: {1} - Nombre: {2} - Peticiones: {3} - Quejas/Reclamaciones {4}";
            Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            Console.WriteLine();
        }

        /// <summary>
        /// Muestra los datos de forma detallada
        /// </summary>
        private void DatosDetallado()
        {
            string[] datos = new string[8] { ID_Cliente.ToString(), DNIc, nombre, Direccion, Telefono.ToString(), CuentaBancaria, historial.NumTrabajos.ToString(), historial.NumQuejas.ToString() };
            string esquema = "ID: {0}\nDNI: {1}\nNombre: {2}\nDirección: {3}\nTelefono: {4}\nCuenta Bancaria: {5}\nNumero de peticiones {6}\nNumero de Quejas o Reclamaciones: {7}";
            Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            Console.WriteLine();
        }

        /// <summary>
        /// Metodo que limpia la linea (en la que se ejecuta) de la consola
        /// </summary>
        private void LimpiarLinea()
        {
            int lineaActual = Console.CursorTop - 1;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, lineaActual);
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el ultimo tecnico asignado al cliente
        /// </summary>
        public string Tecnico
        {
            get
            {
                if (ultimoTecnico == null)
                {
                    return "No asignado";
                }
                else
                {
                    return ultimoTecnico;
                }
            }
        }

        /// <summary>
        /// Propiedad que devuelve el DNI del cliente
        /// </summary>
        public string DNI
        {
            get
            {
                return DNIc;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el Nombre del cliente
        /// </summary>
        public string Nombre
        {
            get
            {
                return nombre;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el ID del cliente
        /// </summary>
        public int ID
        {
            get
            {
                return ID_Cliente;
            }
        }
        #endregion

        #region Indizadores
        #endregion
    }
}
