﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Drawing;
using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema1
{
    class Historial
    {
        #region Atributos
        List<Peticion> ListaPeticiones = new List<Peticion>();
        List<string> ListaActuaciones = new List<string>();
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor de la clase Historial
        /// </summary>
        public Historial()
        {

        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Método que agrega un nuevo trabajo a la lista de peticiones
        /// </summary>
        /// <param name="descripcion">Descripción del trabajo</param>
        /// <returns></returns>
        public string AgregarPeticion(string descripcion)
        {
            DateTime fecha = DateTime.Now;
            string id = "T" + (ListaPeticiones.Count + 1).ToString();
            Peticion peticion = new Peticion(fecha.ToString("dd/MM/yyyy HH:mm"), "Trabajo", id,descripcion);
            ListaPeticiones.Add(peticion);
            return id;
        }

        /// <summary>
        /// Método que agrega una nueva queja a la lista de peticiones
        /// </summary>
        /// <param name="descripcion">Descripción de la queja</param>
        /// <returns></returns>
        public string AgregarQueja(string descripcion)
        {
            DateTime fecha = DateTime.Now;
            string id = "Q" + (ListaPeticiones.Count + 1).ToString();
            Peticion peticion = new Peticion(fecha.ToString("dd/MM/yyyy HH:mm"), "Queja", id,descripcion);
            ListaPeticiones.Add(peticion);
            return id;
        }

        /// <summary>
        /// Método que genera una nueva actuación y la agrega a la lista de actuaciones
        /// </summary>
        /// <param name="autor"></param>
        /// <param name="id"></param>
        /// <param name="tipo"></param>
        /// <param name="mensaje"></param>
        public void RealizarActuacion(string autor,string id,string tipo,string mensaje)
        {
            DateTime fecha = DateTime.Now;
            string actuacion = string.Concat("{0} ",id," - {1} ",tipo," - {2} ", fecha.ToString("dd/MM/yyyy HH:mm"), " - {3} ", autor," - {4} ",mensaje);
            ListaActuaciones.Add(actuacion);
        }

        /// <summary>
        /// Método que devuelve la lista de peticiones de un cliente
        /// </summary>
        /// <returns>Lista de peticiones del cliente</returns>
        public List<Peticion> ObtenerPeticiones()
        {
            return ListaPeticiones;
        }

        /// <summary>
        /// Método que escribe en consola el historial completo del cliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="id"></param>
        /// <param name="tecnico"></param>
        public void MostrarHistorial(string cliente,int id,string tecnico)
        {
            Console.WriteLine("Mostrando el historial del cliente {0}", cliente);
            ListarDatos(cliente, id, tecnico);
            ListarPeticiones();
            ListarActuaciones();
        }
        #endregion

        #region Metodos privados
        /// <summary>
        /// Lista los datos del cliente de forma detallada
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="id"></param>
        /// <param name="tecnico"></param>
        private void ListarDatos(string cliente, int id, string tecnico)
        {
            string[] datos = new string[] { id.ToString(), cliente, tecnico, NumTrabajos.ToString(), NumQuejas.ToString() };
            string esquema = "ID: {0}\nNombre: {1}\nUltimo tecnico asignado: {2}\nNumero de peticiones de trabajo: {3}\nNumero de quejas o reclamaciones: {4}";
            Console.WriteLine("\t=== Datos Generales ===",Color.Cyan);
            Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            Console.WriteLine();
        }

        /// <summary>
        /// Lista las peticiones (Trabajos y Quejas) del cliente
        /// </summary>
        private void ListarPeticiones()
        {
            Console.WriteLine("\t=== Peticiones ===",Color.Cyan);
            foreach (Peticion peticion in ListaPeticiones)
            {
                string[] datos = new string[5] { peticion.ID, peticion.Tipo, peticion.Fecha, peticion.Estado, peticion.Descripcion };
                string esquema = "ID: {0} - Tipo: {1} - Fecha de solicitud: {2} - Estado: {3} - Descripción: {4}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Lista las actuaciones (mensajes realizados) del cliente
        /// </summary>
        private void ListarActuaciones()
        {
            string[] estructura = new string[5] { "Peticion:", "Tipo:", "Fecha:", "Autor:", "Mensaje:" };
            Console.WriteLine("\t=== Actuaciones ===",Color.Cyan);
            for (int i = 0;i<ListaActuaciones.Count;i++)
            {
                Console.WriteLineFormatted(ListaActuaciones[i],Color.LightGreen, Color.White,estructura);
            }
            Console.WriteLine();
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Propiedad que devuelve el numero de trabajos del cliente
        /// </summary>
        public int NumTrabajos
        {
            get
            {
                int contador=0;
                for (int i = 0;i<ListaPeticiones.Count;i++)
                {
                    if (ListaPeticiones[i].Tipo== "Trabajo")
                    {
                        contador++;
                    }
                }
                return contador;
            }
        }

        /// <summary>
        /// Propiedad que devuelve el numero de quejas del cliente
        /// </summary>
        public int NumQuejas
        {
            get
            {
                int contador = 0;
                for (int i = 0; i < ListaPeticiones.Count; i++)
                {
                    if (ListaPeticiones[i].Tipo == "Queja")
                    {
                        contador++;
                    }
                }
                return contador;
            }
        }

        #endregion

        #region Indizadores
        #endregion
    }
}
