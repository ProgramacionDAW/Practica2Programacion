﻿
/*********************************
   Autor: Alberto Caro Gallego
   Fecha creación:      21/01/2017
   Última modificación: 04/03/2017
   Versión: 1.00
 ***********************************/

using System;
using System.Drawing;
using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Problema1
{
    class MenuCal
    {
        #region Atributos
        char Opcion;
        #endregion

        #region Constructores
        /// <summary>
        /// Constructor base del menú de Calidad
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public MenuCal(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            MostrarMenu(ListaClientes, ListaTecnicos);
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Método que muestra el menú de Calidad
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public void MostrarMenu(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            do
            {
                EscribirMenu();
                Console.Write("\nPulse una opción: ");
                while (!char.TryParse(Console.ReadLine(), out Opcion))
                {
                    LimpiarLinea();
                    Console.Write("Pulse una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nPulse una opción: ");
                }
                switch (Opcion)
                {
                    case '1':
                        VerQuejas(ListaClientes);
                        break;
                    case '2':
                        AsignarQueja(ListaClientes,ListaTecnicos);
                        break;
                    case '3':
                        VerHistorialCli(ListaClientes);
                        break;
                    case '4':
                        VerListaClientes(ListaClientes);
                        break;
                }
            } while (Opcion != '0');
        }

        /// <summary>
        /// Método que muestra las quejas de un cliente concreto
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerQuejas(List<Cliente> ListaClientes)
        {
            Console.Title = "Listar quejas";
            Console.Clear();
            if (ListaClientes.Count <= 0)
            {
                Console.WriteLine("No hay ningun cliente registrado. Volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            List<Peticion> Peticiones = historial.ObtenerPeticiones();
            Console.Clear();
            Console.WriteLine("Mostrando las quejas o reclamaciones del cliente {0}", cliente.Nombre);
            if (ContarQuejas(Peticiones)==0)
            {
                Console.WriteLine("El cliente seleccionado no tiene quejas, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            else
            {
                ListarQuejas(Peticiones);
                Console.WriteLine("Pulsa enter para volver...");
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Método que asigna una queja a un tecnico y la resuelve
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="ListaTecnicos"></param>
        public void AsignarQueja(List<Cliente> ListaClientes, List<Tecnico> ListaTecnicos)
        {
            Console.Title = "Asignar Queja";
            Console.Clear();
            if (ListaClientes.Count<=0)
            {
                Console.WriteLine("No hay ningun cliente registrado. Volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            List<Peticion> lista = historial.ObtenerPeticiones();
            Console.Clear();
            if (ContarQuejas(lista) == 0)
            {
                Console.WriteLine("El cliente seleccionado no tiene quejas, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            if (ContarQuejasAct(lista) == 0)
            {
                Console.WriteLine("El cliente seleccionado no tiene quejas activas, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Peticion queja = SeleccionarQueja(lista);
            while (queja==null)
            {
                Console.WriteLine("ID invalida, pulse intro para volver a escribir el ID...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                queja = SeleccionarQueja(lista);
            }
            Tecnico tecnico = BuscarTecnico(ListaTecnicos, cliente.Tecnico);
            if (tecnico!=null)
            {
                if (tecnico.Estado == "Libre")
                {
                    historial.RealizarActuacion(tecnico.Nombre, queja.ID, queja.Tipo, "Queja finalizada.");
                }
                else
                {
                    historial.RealizarActuacion("Dirección", queja.ID, queja.Tipo, "Queja finalizada por la Dirección.");
                }
            }
            else
            {
                historial.RealizarActuacion("Dirección", queja.ID, queja.Tipo, "Queja finalizada por la Dirección.");
            }
            queja.FinalizarTrabajo();
        }

        /// <summary>
        /// Método que muestra la lista de clientes en 2 formatos distintos
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerListaClientes(List<Cliente> ListaClientes)
        {
            if (ListaClientes.Count <= 0)
            {
                Console.Clear();
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            char opcion;
            do
            {
                EscribirMenuVer();
                string formato = "Ninguno";
                while (!char.TryParse(Console.ReadLine(), out opcion))
                {
                    LimpiarLinea();
                    Console.Write("Seleccione una opción: ");
                    Console.Write("Dato introducido no valido.",Color.Red);
                    Thread.Sleep(2000);
                    LimpiarLinea();
                    Console.Write("\nSeleccione una opción: ");
                }
                switch (opcion)
                {
                    case '1':
                        formato = "Detallado";
                        ListarClientes(ListaClientes, formato);
                        break;
                    case '2':
                        formato = "Simplificado";
                        ListarClientes(ListaClientes, formato);
                        break;
                }
            } while (opcion != '0');
        }

        /// <summary>
        /// Método que muestra el historial de un cliente concreto
        /// </summary>
        /// <param name="ListaClientes"></param>
        public void VerHistorialCli(List<Cliente> ListaClientes)
        {
            Console.Title = "Historial de Cliente";
            Console.Clear();
            if (ListaClientes.Count <= 0)
            {
                Console.WriteLine("No hay clientes registrados, volviendo...",Color.Red);
                Thread.Sleep(3000);
                return;
            }
            Cliente cliente = SeleccionarCliente(ListaClientes);
            while (cliente == null)
            {
                Console.WriteLine("Cliente invalido, pulse intro para volver a seleccionar un cliente...",Color.Red);
                Console.ReadLine();
                Console.Clear();
                cliente = SeleccionarCliente(ListaClientes);
            }
            Historial historial = cliente.ObtenerHistorial();
            Console.Clear();
            historial.MostrarHistorial(cliente.Nombre, cliente.ID, cliente.Tecnico);
            Console.WriteLine("Pulsa enter para volver..");
            Console.ReadLine();
        }
        #endregion

        #region Metodos privados
        /// <summary>
        /// Método que escribe por consola el menú de calidad
        /// </summary>
        private void EscribirMenu()
        {
            Console.Title = "Menú de Calidad";
            Console.Clear();
            Console.WriteLine("=== Menú de Calidad ===");
            Console.WriteLine("1 - Ver quejas");
            Console.WriteLine("2 - Finalizar queja");
            Console.WriteLine("3 - Ver Historial de cliente");
            Console.WriteLine("4 - Ver Clientes");
            Console.WriteLine("0 - Volver");
        }

        /// <summary>
        /// Método que nos permite seleccionar un cliente concreto
        /// </summary>
        /// <param name="lista">Lista de Clientes</param>
        /// <returns>Devuelve el cliente en caso de que exista</returns>
        private Cliente SeleccionarCliente(List<Cliente> lista)
        {
            int seleccion;
            int contador = 0;
            Console.WriteLine("Mostrando listado de clientes");
            for (int i = 0; i < lista.Count; i++)
            {
                Cliente cliente = lista[i];
                string[] datos = new string[3] { (i + 1).ToString(), cliente.DNI, cliente.Nombre };
                string esquema = "Numero: {0} - DNI: {1} - Nombre: {2}";
                Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
                contador++;
            }
            Console.Write("\nEscriba el numero del cliente a seleccionar: ");
            while (!int.TryParse(Console.ReadLine(), out seleccion))
            {
                LimpiarLinea();
                Console.Write("Escriba el numero del cliente a seleccionar: ");
                Console.Write("Dato introducido no valido.",Color.Red);
                Thread.Sleep(2000);
                LimpiarLinea();
                Console.Write("\nEscriba el numero del cliente a seleccionar: ");
            }
            if (seleccion<1 || seleccion>contador)
            {
                return null;
            }
            seleccion--;
            return lista[seleccion];
        }

        /// <summary>
        /// Método que nos permite seleccionar una queja concreta
        /// </summary>
        /// <param name="lista">Lista de Peticiones</param>
        /// <returns>Devuelve la queja en caso de que exista</returns>
        private Peticion SeleccionarQueja(List<Peticion> lista)
        {
            Console.WriteLine("Mostrando listado de quejas");
            for (int i = 0; i < lista.Count; i++)
            {
                Peticion peticion = lista[i];
                if (peticion.Tipo=="Queja" && peticion.Estado== "Activa")
                {
                    string[] datos = new string[3] { peticion.ID,peticion.Tipo, peticion.Descripcion };
                    string esquema = "ID: {0} - Tipo: {1} - Descripción: {2}";
                    Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
                }
            }
            Console.Write("\nEscriba el ID de la queja a seleccionar: ");
            string ID = Console.ReadLine();
            foreach (Peticion peticion in lista)
            {
                if (peticion.ID == ID)
                {
                    return peticion;
                }
            }
            return null;
        }

        /// <summary>
        /// Muestra la lista de quejas de un cliente
        /// </summary>
        /// <param name="lista">Lista de Peticiones</param>
        private void ListarQuejas(List<Peticion>lista)
        {
            foreach (Peticion peticion in lista)
            {
                if (peticion.Tipo== "Queja")
                {
                    string[] datos = new string[5] { peticion.ID, peticion.Tipo, peticion.Fecha, peticion.Estado, peticion.Descripcion };
                    string esquema = "ID: {0} - Tipo: {1} - Fecha de solicitud: {2} - Estado: {3} - Descripción: {4}";
                    Console.WriteLineFormatted(esquema, Color.White, Color.LightGreen, datos);
                }
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Método que cuenta el numero de quejas de un cliente para saber si tiene alguna
        /// </summary>
        /// <param name="lista">Lista de Peticiones</param>
        /// <returns>Cantidad de quejas</returns>
        private int ContarQuejas(List<Peticion> lista)
        {
            int contador = 0;
            foreach (Peticion peticion in lista)
            {
                if (peticion.Tipo == "Queja")
                {
                    contador++;
                }
            }
            return contador;
        }

        /// <summary>
        /// Método que cuenta el numero de quejas activas para saber si tiene alguna
        /// </summary>
        /// <param name="lista"></param>
        /// <returns>Cantidad de quejas activas</returns>
        private int ContarQuejasAct(List<Peticion> lista)
        {
            int contador = 0;
            foreach (Peticion peticion in lista)
            {
                if (peticion.Tipo == "Queja" && peticion.Estado=="Activa")
                {
                    contador++;
                }
            }
            return contador;
        }

        /// <summary>
        /// Método que busca un tecnico por su nombre
        /// </summary>
        /// <param name="ListaTecnicos"></param>
        /// <param name="nombre"></param>
        /// <returns>Devuelve el técnico en caso de que exista</returns>
        private Tecnico BuscarTecnico(List<Tecnico> ListaTecnicos,string nombre)
        {
            foreach (Tecnico tecnico in ListaTecnicos)
            {
                if (tecnico.Nombre==nombre)
                {
                    return tecnico;
                }
            }
            return null;
        }

        /// <summary>
        /// Método que escribe el menú de selección de vista
        /// </summary>
        private void EscribirMenuVer()
        {
            Console.Title = "Seleccion de vista";
            Console.Clear();
            Console.WriteLine("\t=== Tipo de vista ===");
            Console.WriteLine("1. Detallada");
            Console.WriteLine("2. Simplificada");
            Console.WriteLine("0. Volver");
            Console.Write("\nSeleccione una opción: ");
        }

        /// <summary>
        /// Método que muestra la lista de clientes en 2 formatos distintos
        /// </summary>
        /// <param name="ListaClientes"></param>
        /// <param name="formato"></param>
        private void ListarClientes(List<Cliente> ListaClientes, string formato)
        {
            Console.Clear();
            Console.WriteLine("Mostrando lista de clientes en formato {0}", formato);
            Console.WriteLine();
            switch (formato)
            {
                case "Simplificado":
                    foreach (Cliente cliente in ListaClientes)
                    {
                        cliente.MostrarDatos(formato);
                    }
                    break;
                case "Detallado":
                    foreach (Cliente cliente in ListaClientes)
                    {
                        cliente.MostrarDatos(formato);
                    }
                    break;
            }
            Console.WriteLine("Pulse enter para continuar...");
            Console.ReadLine();
        }

        /// <summary>
        /// Metodo que limpia la linea (en la que se ejecuta) de la consola
        /// </summary>
        private void LimpiarLinea()
        {
            int lineaActual = Console.CursorTop-1;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, lineaActual);
        }
        #endregion

        #region Propiedades

        #endregion

        #region Indizadores

        #endregion
    }
}
